fnux.model.net
================

This is the FNUX model made easy - probably the only project of its kind and purpose: To make it easier for vendors in the danish healthcare sector to use exchange patient related data in the FNUX format.

This project fetches the current versions of the FNUX schemas, generates classes from the xsd's and parses them into a .NET library - thereby making it way easier to parse the xml. Just call the script.bat from a **Visual Studio** ***Developer Command Prompt*** and you should have a library in just about no time.

Happy codin'.