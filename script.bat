rmdir /Q /S generated
rmdir /Q /S rep.oio.dk
rmdir /Q /S Schemas
svn export http://svn.medcom.dk/svn/drafts/FNUX/Schemas
wget\wget.exe -m -np http://rep.oio.dk/xkom.dk/xml/schemas/2006/01/06 -A xsd,xml
wget\wget.exe -m -np http://rep.oio.dk/xkom.dk/xml/schemas/2005/03/15 -A xsd,xml
wget\wget.exe -m -np http://rep.oio.dk/ebxml/xml/schemas/dkcc/2003/ -A xsd,xml
wget\wget.exe -m -np http://rep.oio.dk/ebxml/xml/schemas/dkcc/2005/ -A xsd,xml
wget\wget.exe -m -np http://rep.oio.dk/cpr.dk/xml/schemas/core/2005/ -A xsd,xml
wget\wget.exe -m -np http://rep.oio.dk/cvr.dk/xml/schemas/2005/03/22/ -A xsd,xml
wget\wget.exe -m -np http://rep.oio.dk/itst.dk/xml/schemas/2006/01/17/ -A xsd,xml
wget\wget.exe -m -np http://rep.oio.dk/itst.dk/xml/schemas/2005/01/10/ -A xsd,xml
mkdir generated
xsd /p:schemas.xml /c /n:fnux /o:generated
rename generated\xhtml1-strict-simplified.cs fnux.model.cs
csc /target:library /out:Fnux.Model.dll /debug generated\fnux.model.cs 
TIMEOUT 10
rmdir /Q /S generated
rmdir /Q /S rep.oio.dk
rmdir /Q /S Schemas